import axios from 'axios';


const state = {
  recipes: [],
  category: [],
  // searchRecipes: []
}

const getters = {

  recipes: state => {

    return state.recipes

  },

  searchRecipes: (state) => (keyword) => {

    const word = keyword.toLowerCase().split(' ')

    return state.recipes.filter(function(item) {
      return word.every(function(el) {
        return item.title.toLowerCase().indexOf(el) > -1
      })
    })

  },

  filterRecipes: (state) => (param) => {

    var data = state.recipes

    if (param.time !== "") {
      data = data.filter(function(item) {
        return item.time.toLowerCase().indexOf(param.time) > -1
      })
    }

    if (param.serving !== "") {
      data = data.filter(function(item) {
        return item.serving.toLowerCase().indexOf(param.serving) > -1
      })
    }

    if (param.category.length > 0) {
      const category = JSON.parse(JSON.stringify(param.category));
      var filteredDataByCategory = []
      for (let i = 0; i < data.length; i++) {
        if (category.includes(data[i].category)) {
          filteredDataByCategory.push(data[i])
        }
      }

      data = filteredDataByCategory
    }

    return data

  },

  category: state => {
    return state.category
  },

}

const actions = {

  getAllRecipes(context) {

    axios.get(`https://private-b03d3a-wowbid1.apiary-mock.com/recipes`)
    .then(response => {
      context.commit('SET_RECIPES', response.data)
    })
    .catch(e => {
      if (e.response) {
        console.log(e.response.data)
        console.log(e.response.status)
        console.log(e.response.headers)
      }
    })
  },

  getAllCategory(context) {

    axios.get(`https://private-b03d3a-wowbid1.apiary-mock.com/categories`)
    .then(response => {
      context.commit('SET_CATEGORY', response.data)
    })
    .catch(e => {
      if (e.response) {
        console.log(e.response.data)
        console.log(e.response.status)
        console.log(e.response.headers)
      }
    })
  },

  // searchRecipess(context, keyword) {

  //   const recipes = JSON.parse(localStorage.getItem('my-app')).discover.recipes

  //   const word = keyword.toLowerCase().split(' ')

  //   const result = recipes.filter(function(item) {
  //     return word.every(function(el) {
  //       return item.title.toLowerCase().indexOf(el) > -1
  //     })
  //   })

  //   context.commit('SET_SEARCH_RECIPES', result)

  // }

};

const mutations = {
  SET_RECIPES(state, recipes) {
    state.recipes = recipes
  },

  SET_CATEGORY(state, category) {
    state.category = category
  },

  // SET_SEARCH_RECIPES(state, searchRecipes) {
  //   state.searchRecipes = searchRecipes
  // },
};

export default {
  state,
  getters,
  actions,
  mutations
}