import Vue from 'vue'
import VueRouter from 'vue-router'
import Discover from '../views/discover/Discover.vue'
import Search from '../views/search/Search.vue'
import Filter from '../views/filter/Filter.vue'

Vue.use(VueRouter)

  const routes = [
    {
      path: '/',
      name: 'Discover',
      component: Discover
    },
    {
      path: '/recipes',
      name: 'Recipes',
      component: Discover
    },
    {
      path: '/favorites',
      name: 'Favorites',
      component: Discover
    },
    {
      path: '/shopping-list',
      name: 'Shopping List',
      component: Discover
    },
    {
      path: '/profile',
      name: 'Profile',
      component: Discover
    },
    {
      path: '/setting',
      name: 'Settings',
      component: Discover
    },

    {
      path: '/search',
      name: 'Search',
      component: Search
    },
    {
      path: '/filter',
      name: 'Filter',
      component: Filter
    },


  // {
  //   path: '/about',
  //   name: 'About',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
